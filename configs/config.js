module.exports = {
    llave: "encompassgsb2020*",
    urlBase: "apiEncompass",
    //este es el requets general que se debe lanzar para consumis las apis de busqueda
    body: {
        "pais": "",
        "codigoProducto": "",
        "marca": "",
        "descripcion": "",
        "usuario": "",
        "buscador": ""
    },
    //este objeto es el general para respondr un error
    error: {
        "status": {
            "httpCode":"",
            "errorCode": "",
            "errorMessage": ""
        }
    },
    success: {
        "status": {
            "errorCode": "",
            "errorMessage": ""
        }
    }
};





