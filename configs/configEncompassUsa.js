var jsonUser = 'GLOBAL';
var jsonPassword = 'G33V76HS3D5';
//var customerNumber = '211162';
//var customerPassword = 'encompass1.';
var customerNumber = '156400';
var customerPassword = '4706';

module.exports = {
    configuracion: {
        "headers": { "content-type": "application/json" },
        "rutaLogsResponse": "logs/logsResponse/",
        "rutaLogsConexion": "logs/logsConexion/",
        "archivoLectura": "carpetaLectura/objetoLectura.json",
        "guardarLogs":"0"
    },
    apis:  {
        "partsInformation": {
            "nombreruta":"informacionProducto",
            "url":"https://encompass.com/restfulservice/partsInformation",
            //Este body es lo que se necesita para consumir la api de partsInformation USA
            "body": {
                "settings": {
                    "jsonUser": jsonUser,
                    "jsonPassword": jsonPassword,
                    "customerNumber": customerNumber,
                    "customerPassword": customerPassword,
                    "programName": "JSON.ITEM.INFORMATION"
                  },
                "data": {
                    "basePN": "",
                    "searchMfgCode": "",
                    "searchPartNumber": "",
                    "bidNumber": ""
                }
            }
        },
        "search": {
            "nombreruta":"buscar",
            "url":"https://encompass.com/restfulservice/search",
            //Este body es lo que se necesita para consumir la api de search USA
            "body": {
                "settings": {
                  "jsonUser": jsonUser,
                  "jsonPassword": jsonPassword
                },
                "data": {
                  "searchTerm": "",
                  "mode": "Value",
                  "limitBrand": ""
                }
              }
        },
    }
}