const express = require('express'),
    bodyParser = require('body-parser'),
    jwt = require('jsonwebtoken'),
    config = require('./configs/config'),
    app = express();

app.set('llave', config.llave);

app.use(bodyParser.urlencoded({ extended: true }));

app.use(bodyParser.json());

/*app.listen(3000, () => {
    console.log('Servidor iniciado en el puerto 3000')
});*/
//asignamos puerto, por si esta en la nube le decimos que escuche el puerto que este disponible si no por el 3000
app.set('port', process.env.PORT ||  3000);

app.get('/'+config.urlBase, function (req, res) {
    res.send('Inicio');
});

//RUTAS
require('./rutas/sesiones')(app);
require('./rutas/partsInformation')(app);
require('./rutas/search')(app);
require('./rutas/mx/inventariomx')(app);

app.listen(app.get('port'),()=>{
    console.log('Server en el puerto ', app.get('port'));
});


///pm2 stop api-encompass
///pm2 restart api-encompass
//pm2 start index.js --name "api-encompass"
