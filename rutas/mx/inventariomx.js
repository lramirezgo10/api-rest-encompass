jwt = require('jsonwebtoken');
express = require('express')
const rutasProtegidas = express.Router();
conEncompass = require('../../models/usa/conexionEncompassUsa');
login = require('../../models/login');
config = require('../../configs/config');
configEncUsa = require('../../configs/configEncompassUsa');
modelInventarioMx = require('../../models/mx/modelinventariomx');

//modulo exportado a las rutas
module.exports = function (app) {
    rutasProtegidas.use((req, res, next) => {
        //Validamos que el usuario exista en la base de datos
        var parametros = {};
        parametros.usuario = req.body.usuario;
        parametros.token = req.headers['access-token'];
        parametros.estatus = "a";
        login.comprobarUsuario(parametros).then((response) => {
            const token = req.headers['access-token'];
            if (token) {
                jwt.verify(token, app.get('llave'), (err, decoded) => {
                    if (err) {
                        return res.json({ mensaje: 'Token inválida' });
                    } else {
                        req.decoded = decoded;
                        next();
                    }
                });
            } else {
                res.status(400);
                res.send({
                    mensaje: 'Token no proveída.'
                });
            }
        }).catch((error) => {
            config.error.status.errorCode = '1001';
            config.error.status.errorMessage = 'El usuario no es valido';
            res.status(400);
            res.send(config.error);
        })
    });
    async function asyncForEach(array, callback) {
        for (let index = 0; index < array.length; index++) {
            await callback(array[index], index, array);
        }
    };
    app.post('/' + config.urlBase + '/insertInventariomx', rutasProtegidas, (req, res) => {
        var responseLog = []

        if (req.body.pais != 'MX') {
            config.error.status.errorCode = '1000';
            config.error.status.errorMessage = 'Pais no valido';
            res.status(400);
            res.send(config.error);
        };
        if (req.body.pais == 'MX') {
            var parametros = {};
            const iterarProductos = async () => {
                await asyncForEach(req.body.productos, async (elementProductos) => {
                    var parametros = {};
                    var diferencia = false;
                    parametros.codigoProducto = elementProductos.codigoproducto;
                    await modelInventarioMx.producto(parametros).then(async (response) => {

                        //Validamos diferencias en la información
                        if (response.datos[0].codigoproducto != elementProductos.codigoproducto) {
                            diferencia = true;
                        };
                        if (response.datos[0].descripcionproducto != elementProductos.descripcionproducto) {
                            diferencia = true;
                        };
                        if (response.datos[0].marca != elementProductos.marca) {
                            diferencia = true;
                        };
                        if (response.datos[0].descripcionmarca != elementProductos.descripcionmarca) {
                            diferencia = true;
                        };
                        if (response.datos[0].precio != elementProductos.precio) {
                            diferencia = true;
                        };
                        if (response.datos[0].disponibilidad != elementProductos.disponibilidad) {
                            diferencia = true;
                        };
                        if (response.datos[0].imagen != elementProductos.imagen) {
                            diferencia = true;
                        };
                        if (diferencia) {
                            await modelInventarioMx.updateInvetarioMX(elementProductos).then((response) => {
                                objResponseLog = {
                                    status: {
                                        httpCode:'',
                                        errorCode: '100',
                                        errorMessage: errorMessage = response.sqlMensaje,
                                        codigoProducto: codigoProducto = elementProductos.codigoproducto,
                                        accion: 'Update información de producto'
                                    }
                                };
                                responseLog.push(objResponseLog);
                                //res.send(config.success);
                            }).catch((error) => {
                                objResponseLog = {
                                    status: {
                                        httpCode:'',
                                        errorCode: '1002',
                                        errorMessage: errorMessage = error.sqlMensaje,
                                        codigoProducto: codigoProducto = elementProductos.codigoproducto,
                                        accion: 'Update información de producto'
                                    }
                                };
                                responseLog.push(objResponseLog);
                            })
                        };

                    }).catch(async (error) => {
                        //console.info("entre a error");
                        //entra aqui en caso de NO encontrar registros coincidentes hace insert
                        await modelInventarioMx.insertInventarioMX(elementProductos).then(
                            (response) => {
                                objResponseLog = {
                                    status: {
                                        httpCode:'',
                                        errorCode: '100',
                                        errorMessage: errorMessage = response.sqlMensaje,
                                        codigoProducto: codigoProducto = elementProductos.codigoproducto,
                                        accion: 'Insert producto'
                                    }
                                };
                                responseLog.push(objResponseLog);
                            }).catch((error) => {
                                objResponseLog = {
                                    status: {
                                        httpCode:'',
                                        errorCode: '1002',
                                        errorMessage: errorMessage = error.sqlMensaje,
                                        codigoProducto: codigoProducto = elementProductos.codigoproducto,
                                        accion: 'Insert producto'
                                    }
                                };
                                responseLog.push(objResponseLog);
                            })
                    })

                });
                if (responseLog.length > 0) {
                    res.send(responseLog);
                } else {
                    res.send({
                        status: {
                            "errorCode": "1003",
                            "errorMessage": "No hubo actualizaciones al inventario"
                        }

                    });

                }

            };
            iterarProductos();
        };
    });
    app.post('/' + config.urlBase + '/inventariomx', rutasProtegidas, (req, res) => {
        if (req.body.pais != 'MX') {
            config.error.status.errorCode = '1000';
            config.error.status.errorMessage = 'Pais no valido';
            res.status(400);
            res.send(config.error);
        };
        if (req.body.pais == 'MX') {
            modelInventarioMx.inventario().then(async (response) => {
                //console.info(response);
                res.send(response);
            }).catch(async (error) => {
                //console.info(error);
                config.error.status.errorCode = '1004';
                config.error.status.errorMessage = error.sqlMensaje;
                res.send(config.error);
            });
        }
    });

}