
jwt = require('jsonwebtoken');
express = require('express')
const rutasProtegidas = express.Router();

module.exports = function (app) {
    
    //peticion post para validar que el usuario y contraseña en la BD existan, de ser asi le regresara un token para las posteriores consultas
    app.post('/apiEncompass/generarToken', (req, res) => {
        if (req.body.usuario === "luis.ramirez") {
            const payload = {
                check: true
            };
            const token = jwt.sign(payload, app.get('llave'), {
               // expiresIn: 10
            });
            res.json({
                mensaje: 'Autenticación correcta',
                token: token
            });
        } else {
            res.json({ mensaje: "Usuario no valido" })
        }
    });
}


