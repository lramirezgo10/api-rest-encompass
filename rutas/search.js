jwt = require('jsonwebtoken');
express = require('express')
const rutasProtegidas = express.Router();
conEncompass = require('../models/usa/conexionEncompassUsa');
login = require('../models/login');
config = require('../configs/config');
buscadorMx = require('../models/mx/modelBuscar');


module.exports = function (app) {

    rutasProtegidas.use((req, res, next) => {
        //Validamos que el usuario exista en la base de datos
        var parametros = {};
        parametros.usuario = req.body.usuario;
        parametros.token = req.headers['access-token'];
        parametros.estatus = "a";
        parametros.tipo = "sis";
        login.comprobarUsuario(parametros).then((response) => {
            const token = req.headers['access-token'];
            if (token) {
                jwt.verify(token, app.get('llave'), (err, decoded) => {
                    if (err) {
                        return res.json({ mensaje: 'Token inválida' });
                    } else {
                        req.decoded = decoded;
                        next();
                    }
                });
            } else {
                res.status(400);
                res.send({
                    mensaje: 'Token no proveída.'
                });
            }
        }).catch((error) => {
            config.error.status.errorCode = '1001';
            config.error.status.errorMessage = 'El usuario no es valido';
            res.status(400);
            res.send(config.error);
        })
    });

    app.post('/' + config.urlBase + '/' + configEncUsa.apis.search.nombreruta, rutasProtegidas, (req, res) => {
        //seteamos los parametros con los de estados unidos

        configEncompassUsa.apis.search.body.data.searchTerm = req.body.buscador;

        if (req.body.pais != 'USA' && req.body.pais != 'MX') {
            config.error.status.errorCode = '1000';
            config.error.status.errorMessage = 'Pais no valido';
            res.status(400);
            res.send(config.error);
        };
        if (req.body.pais == 'USA') {
            conEncompass.peticion(configEncompassUsa.apis.search.body, configEncUsa.apis.search.url).then((response) => {
                res.send(response);
            }).catch((error) => {
                res.status(400);
                res.send(error);
            })
        };
        if (req.body.pais == 'MX') {
            buscadorMx.busqueda(req.body).then(async (response) => {
                //console.info(response);
                res.send(response);
            }).catch(async (error) => {
                //console.info(error);
                config.error.status.errorCode = '1004';
                config.error.status.errorMessage = error.sqlMensaje;
                res.send(config.error);
            });
        };
    });

}