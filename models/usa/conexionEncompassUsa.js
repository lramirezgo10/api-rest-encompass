configEncompassUsa = require('../../configs/configEncompassUsa');
var Request = require("request");
let dataEncompassUsa = {};

dataEncompassUsa.peticion = (body, urlApi) => {
    return new Promise((resolve, reject) => {
        Request.post({
            "headers": configEncompassUsa.configuracion.headers,
            "url": urlApi,//consumimos el url proporcionado
            "body": JSON.stringify(body) //trasformamos el objeto que sera enviado por metodo post
        }, (error, response, body) => {
            var objResponse = JSON.parse(response.body);
            if (response.errorCode) {
                reject(objResponse);
            } else {
                resolve(objResponse);
            };
        });
    });
};

module.exports = dataEncompassUsa;