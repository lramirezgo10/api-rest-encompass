const MySql = require('../modulos/ejecucionQuerys');

let UserModel = {};
let respuesta = {};

UserModel.comprobarUsuario = (parametros) => {
    return new Promise((resolve, reject) => {
        var query = `SELECT usuario, token, estatus  FROM usuarios `
        MySql.select(query, parametros).then((respuesta) => {
            resolve(respuesta);
        }).catch((error) => {
            reject(error);
        });
    });
};

module.exports = UserModel;