const MySql = require('../../modulos/ejecucionQuerys');

let inventarioModel = {};
let objOrespuesta = {};

inventarioModel.producto = (parametros) => {
  return new Promise((resolve, reject) => {
    var query = `SELECT 
        id,
        codigoproducto,
        descripcionproducto,
        marca,
        descripcionmarca,
        precio,
        disponibilidad,
        imagen,
        usuarioagrego,
        usuariomodifico,
        fechorainsercion,
        fechoramodificacion
        FROM
        inventarioencmx `
    MySql.select(query, parametros).then((respuesta) => {
      resolve(respuesta);
    }).catch((error) => {
      reject(error);
    });
  });
};

inventarioModel.inventario = (parametros) => {
  return new Promise((resolve, reject) => {
    var query = `SELECT 
      id,
      codigoproducto,
      descripcionproducto,
      marca,
      descripcionmarca,
      precio,
      disponibilidad,
      imagen,
      usuarioagrego,
      usuariomodifico,
      fechorainsercion,
      fechoramodificacion
      FROM
      inventarioencmx `
    MySql.select(query, parametros).then((respuesta) => {
      objOrespuesta.status = {};
      objOrespuesta.data = {};
      objOrespuesta.data.parts = [];
      
      objOrespuesta.status.httpcode = '';
      objOrespuesta.status.errorCode = '100';
      objOrespuesta.status.errorMessage = respuesta.sqlMensaje;
      respuesta.datos.forEach(
        function (partsMx) {
          parts = {};
          parts.basePN = '';
          parts.mfgCode = partsMx.marca;
          parts.mfgName = partsMx.descripcionmarca;
          parts.partNumber = partsMx.codigoproducto;
          parts.weight = '';
          parts.length = '';
          parts.width = '';
          parts.height = '';
          parts.htsCode = '';
          parts.oversize = '';
          parts.authorizationRequired = '';
          parts.availability = partsMx.disponibilidad;
          parts.availabilityByLocation =[];
          parts.eta = '';
          parts.listPrice = '';
          parts.corePrice = '';
          parts.partPrice = '';
          parts.totalPrice = partsMx.precio;
          parts.subPart = '';
          parts.allowRTS = '';
          parts.allowIWR = '';
          parts.smallImageURL = partsMx.imagen;

        objOrespuesta.data.parts.push(parts);
        }
      );
      resolve(objOrespuesta);
    }).catch((error) => {
      reject(error);
    });
  });
};

inventarioModel.updateInvetarioMX = (parametros) => {
  return new Promise((resolve, reject) => {
    var query = `
      UPDATE inventarioencmx SET
      ?
      WHERE codigoproducto = ${connection.escape(parametros.codigoproducto)}`

    MySql.crud(query, parametros).then((respuesta) => {

      resolve(respuesta);
    }).catch((error) => {
      reject(error);
    });
  });
};

inventarioModel.insertInventarioMX = (parametros) => {
  return new Promise((resolve, reject) => {
    var query = `insert into inventarioencmx SET ?`
    MySql.crud(query, parametros).then((respuestainsert) => {
      resolve(respuestainsert);
    }).catch((error) => {
      reject(error);
    });
  });
};

module.exports = inventarioModel;