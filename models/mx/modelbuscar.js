const MySql = require('../../modulos/ejecucionQuerys');

let buscarMxModel = {};
let objOrespuesta = {};

buscarMxModel.busqueda = (parametros) => {
    console.info(parametros);
    return new Promise((resolve, reject) => {
        var query = `SELECT * FROM inventarioencmx 
    where (MATCH ( codigoproducto  ) AGAINST ('%` + parametros.buscador + `%' IN BOOLEAN MODE)
    or 
   MATCH ( descripcionproducto) AGAINST ('%` + parametros.buscador + `%' IN BOOLEAN MODE)
    or 
   MATCH ( marca  ) AGAINST ('%` + parametros.buscador + `%' IN BOOLEAN MODE)
     or 
   MATCH ( descripcionmarca  ) AGAINST ('%` + parametros.buscador + `%' IN BOOLEAN MODE))
   or  (codigoproducto = '` + parametros.buscador + `'
   or descripcionproducto = '` + parametros.buscador + `'
   or marca = ''
   or descripcionmarca = '` + parametros.buscador + `' );
    `
        MySql.select(query, parametros, false).then((respuesta) => {
            objOrespuesta.status = {};
            objOrespuesta.data = {};
            objOrespuesta.data.numberOfParts = respuesta.datos.length;
            objOrespuesta.data.numberOfModels = '0';
            objOrespuesta.data.parts = [];

            objOrespuesta.status.httpcode = '';
            objOrespuesta.status.errorCode = '100';
            objOrespuesta.status.errorMessage = respuesta.sqlMensaje;
            respuesta.datos.forEach(
                function (partsMx) {
                    parts = {};
                    parts.basePN = '';
                    parts.mfgCode = partsMx.marca;
                    parts.partNumber = partsMx.codigoproducto;
                    parts.description = partsMx.descripcionproducto;
                    parts.partImage = partsMx.imagen
                    parts.mfgName = partsMx.descripcionmarca;
                    parts.models = [];
                    parts.totalItems = 1;
                    objOrespuesta.data.parts.push(parts);
                }
            );
            resolve(objOrespuesta);
        }).catch((error) => {
            reject(error);
        });
    });
};


module.exports = buscarMxModel;