const MySql = require('../../modulos/ejecucionQuerys');

let informacionProductoModel = {};
let objOrespuesta = {};

informacionProductoModel.busqueda = (parametros) => {
    return new Promise((resolve, reject) => {
        var query = `SELECT * FROM inventarioencmx 
    where (MATCH ( codigoproducto  ) AGAINST ('%` + parametros.codigoProducto + `%' IN BOOLEAN MODE)
    or 
   MATCH ( descripcionproducto) AGAINST ('%` + parametros.descripcion + `%' IN BOOLEAN MODE)
    or 
   MATCH ( marca  ) AGAINST ('%` + parametros.marca + `%' IN BOOLEAN MODE)
     or 
   MATCH ( descripcionmarca  ) AGAINST ('%` + parametros.descripcionmarca + `%' IN BOOLEAN MODE))
   or  (codigoproducto = '` + parametros.codigoProducto + `'
   or descripcionproducto = '` + parametros.descripcion + `'
   or marca = ''
   or descripcionmarca = '` + parametros.descripcionmarca + `' );
    `
        MySql.select(query, parametros, false).then((respuesta) => {
            objOrespuesta.status = {};
            objOrespuesta.data = {};
            objOrespuesta.data.parts = [];

            objOrespuesta.status.httpcode = '';
            objOrespuesta.status.errorCode = '100';
            objOrespuesta.status.errorMessage = respuesta.sqlMensaje;
            respuesta.datos.forEach(
                function (partsMx) {
                    parts = {};
                    parts.basePN = '';
                    parts.mfgCode = partsMx.marca;
                    parts.mfgName = partsMx.descripcionmarca;
                    parts.partNumber = partsMx.codigoproducto;
                    parts.weight = '';
                    parts.length = '';
                    parts.width = '';
                    parts.height = '';
                    parts.htsCode = '';
                    parts.oversize = '';
                    parts.authorizationRequired = '';
                    parts.availability = partsMx.disponibilidad;
                    parts.availabilityByLocation = [];
                    parts.eta = '';
                    parts.listPrice = '';
                    parts.corePrice = '';
                    parts.partPrice = '';
                    parts.totalPrice = partsMx.precio;
                    parts.subPart = '';
                    parts.allowRTS = '';
                    parts.allowIWR = '';
                    parts.smallImageURL = partsMx.imagen;

                    objOrespuesta.data.parts.push(parts);
                }
            );
            resolve(objOrespuesta);
        }).catch((error) => {
            reject(error);
        });
    });
};


module.exports = informacionProductoModel;