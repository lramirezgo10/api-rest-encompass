const conexion = require('../modulos/conexion');
const whereArmado = require('./armadoWhere');

let executeMYSQL = {};


executeMYSQL.select = (query, parametros, armarwhere) => {
    return new Promise((resolve, reject) => {

        ////RECIBIMOS LOS PARAMETROS Y LOS MANDAMOS A MODULO QUE SE ENCARGA DE LA CREACION DEL WHERE
        armarwhere = armarwhere == undefined ? true : armarwhere;
        if (armarwhere) {
            var whereArm;
            whereArm = '';
            whereArm = whereArmado.armar(parametros);
            query = query + whereArm;
        }else{
            query = query
        }
        ///MANDAMOS A AJECUTAR EL QUERY
        conexion.query(query,
            (err, rows) => {
                //EVALUAMOS SI HAY UN ERROR EN LA SINTAXIS DEL QUERY
                if (err) {
                    respuesta = {
                        sqlMensaje: err.sqlMessage,
                        error: true
                    }
                    reject(respuesta);
                }
                else {
                    //EN CASO DE NO HABER ERROR EN LA SINTAXIS EVALUAMOS QUE EL QUERY HAYA REGRESADO ALGUN VALOR
                    if (rows.length < 1) {
                        respuesta = {
                            sqlMensaje: "No se encontro informacion",
                            error: true,
                            datos: rows
                        }
                        reject(respuesta);
                    } else {
                        respuesta = {
                            sqlMensaje: "Busqueda correcta",
                            error: false,
                            datos: rows
                        }
                        resolve(respuesta);
                    }

                };
            });
    });
};

executeMYSQL.crud = (query, parametros) => {
    return new Promise((resolve, reject) => {
        ///MANDAMOS A AJECUTAR EL QUERY
        conexion.query(query, parametros,
            (err, rows) => {
                //EVALUAMOS SI HAY UN ERROR EN LA SINTAXIS DEL QUERY
                if (err) {
                    respuesta = {
                        sqlMensaje: err.sqlMessage,
                        error: true
                    }
                    reject(respuesta);
                }
                else {
                    respuesta = {
                        sqlMensaje: "correcto",
                        error: false,
                        datos: rows
                    }
                    resolve(respuesta);
                };
            });
    });
};
module.exports = executeMYSQL;